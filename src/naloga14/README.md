# 14. Naloga
Napišite program, ki implementira binarno iskanje števila po urejeni tabeli.
Prvo število je dolžina zaporedja, drugo je iskano število. Nato sledi zaporedje števil. Če števila ne najde naj izpiše NAPAKA

Primer vhoda:
```
6 12
2 9 12 17 19 23
```
Primer izhoda: 
```
12
```
