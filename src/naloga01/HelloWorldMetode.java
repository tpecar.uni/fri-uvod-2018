/*
 *  Za namen uvoda v Studij za bruce FRI 2018.
 *  Vsa koda je pod MIT licenco (glej LICENSE.md)
 */
package naloga01;

/**
 * Hello World, le da poklicemo se kaksno svojo metodo poleg main.
 * 
 * Nadalnje branje:
 *  https://www.tutorialspoint.com/java/java_methods.htm
 */
public class HelloWorldMetode {
    
    // Metoda, ki na standardni izhod (System.out) izpise Hello
    // Ker jo klicemo iz main() metode, ki je static, mora tudi ta metoda biti
    // staticna
    public static void hello() {
        // Izpisemo, ostanemo v isti vrstici (print)
        System.out.print("Hello ");
    }
    
    // Dobri stari main
    public static void main(String[] args) {
        hello();
        System.out.println(world(1));
        
        // Ni prepovedano, ce imamo vec stavkov v isti vrstici, je pa manj
        // berljivo.
        hello(); System.out.println(world(-10));
        
        // glede na argumente se izbere overload-ana metoda
        hello(); world();
    }
    
    // Metoda, ki sama po sebi ne izpise nicesar, temvec vrne niz.
    //
    // Vrstni red, v katerem definiramo metode v razredu, je v Javi nepomemben
    // main() bo lahko klical world(), ceprav je world definiran za njim
    public static String world(int argument) {
        
        // Nas izhod bo odvisen od argumenta
        if(argument == 1)
            // Kot izhodno vrednost vrnemo niz
            return "world!";
        else
            return "universe!";
    }
    
    // Metoda, ki je po imenu enaka ze obstojeci metodi world, vendar ima
    // drugacne argumente ter izhodno vrednost.
    //
    // Temu konceptu se ang. rece overloading.
    //
    // Vec o tem na: https://beginnersbook.com/2013/05/method-overloading/
    public static void world() {
        System.out.println("lovely world!");
    }
}
