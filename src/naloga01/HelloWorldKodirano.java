/*
 *  Za namen uvoda v Studij za bruce FRI 2018.
 *  Vsa koda je pod MIT licenco (glej LICENSE.md)
 */
package naloga01;

//import java.util.Arrays;

/**
 * Hello World je lahko implementiran na mnogo nacinov.
 * 
 * Kako zabavna je njegova implementacija, je odvisno le od programerjeve
 * domisljije.
 * 
 * Tu imamo nas "Hello World!" niz shranjen kot niz razlik med ASCII vrednostmi
 * sosednjih znakov.
 */
public class HelloWorldKodirano {
    
    static int[] kodiranHelloWorld = {
        72, -29, -7, 0, -3, 79, -87, 8, -3, 6, 8, 67
    };
    
    // Dobri stari main
    public static void main(String[] args) {
        // Zakodirano tabelo lahko pridobis z zakodiraj
        // Za izpis lahko uporabil Arrays razred iz standardne knjiznice
        // Vec na: https://docs.oracle.com/javase/7/docs/api/java/util/Arrays.html
        //
        //System.out.println(Arrays.toString(zakodiraj("Hello world!")));
        
        System.out.println(dekodiraj(kodiranHelloWorld));
    }
    
    // Metoda, ki zakodira podan niz
    static int[] zakodiraj(String niz) {
        // Ustvarimo tabelo, ki bo hranila nas kodiran niz
        // Pri ustvarjanju tabele moramo navesti njeno dolzino - ta naj bo
        // enaka dolzini niza - to lahko od String objekta pridobimo preko
        // metode length()
        //
        // Glej:
        //  https://docs.oracle.com/javase/tutorial/java/nutsandbolts/arrays.html
        //  https://docs.oracle.com/javase/7/docs/api/java/lang/String.html#length()
        int[] kod = new int[niz.length()];
        
        // prvi znak ostane enak
        // S charAt() vrnemo posamezen znak na podanem mestu iz niza.
        kod[0] = niz.charAt(0);
        
        // tIndeks kot "trenutni indeks"
        for(int tIndeks = 1; tIndeks < niz.length(); tIndeks++)
            kod[tIndeks] = niz.charAt(tIndeks-1) - niz.charAt(tIndeks);
        
        return kod;
    }
    
    // Metoda, ki dekodira podan niz
    static String dekodiraj(int[] kod) {
        // Ustvarimo niz s prvim znakom
        //
        // "" je prazen niz, + operator bo drugi operand (tj. znak) samodejno
        // pretvoril v niz.
        // kod[0] je naceloma stevilo (int) - Javi moramo rocno nakazati, da ga
        // naj tretira kot znak
        String niz = "" + (char)kod[0];
        
        int znak = kod[0];
        // pri tabelah je .length lastnost (lahko bi rekli atribut objekta
        // tabele), ne metoda
        for(int tIndeks = 1; tIndeks < kod.length; tIndeks++) {
            znak = znak - kod[tIndeks];
            niz = niz + (char)znak;
        }
        return niz;
    }
}
