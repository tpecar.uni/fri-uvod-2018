/*
 *  Za namen uvoda v Studij za bruce FRI 2018.
 *  Vsa koda je pod MIT licenco (glej LICENSE.md)
 */
package naloga01;

import java.util.Scanner;

/**
 * Dobri stari Hello World.
 * 
 * Za nadaljnje branje glej:
 *  System razred
 *  https://docs.oracle.com/javase/7/docs/api/java/lang/System.html
 * 
 *  PrintStream razred, ki je skrit pod "System.out"
 *  https://docs.oracle.com/javase/7/docs/api/java/io/PrintStream.html
 * 
 *  InputStream razred, ki je skrit pod "System.in"
 *  https://docs.oracle.com/javase/7/docs/api/java/io/InputStream.html
 * 
 *  Scanner razred, ki je namenjen pretvorbi byte-ov vhodnega toka (System.in) v
 *  specificen tip
 *  https://docs.oracle.com/javase/7/docs/api/java/util/Scanner.html
 */
public class HelloWorld { // razred
    
    // atribut razreda (ang. member)
    //
    // Ker ga uporabljamo znotraj main() metode, ki je staticna, mora tudi
    // atribut biti staticen - staticnim atributom pravimo tudi spremenljivke
    // razreda (class variable / class member)
    //
    // Vec o tem na:
    //  https://docs.oracle.com/javase/tutorial/java/javaOO/classvars.html
    //  https://en.wikipedia.org/wiki/Member_variable
    static String besedilo = "Hello World!";
    
    
    // Metoda (po domace receno funkcija) main razreda HelloWorld
    //
    // main() je nekoliko posebna funkcija, ker se samodejno pozene
    // ob zagonu programa - lahko bi rekli, da jo poklice operacijski sistem
    //
    // Se vedno pa za main veljajo enaka pravila kot za ostale funkcije
    // =========================================================================
    // In sicer:
    //
    // public - pove, da lahko metodo main() klice neka koda izven razreda
    //          (v nasem primeru "operacijski sistem")
    // static - pove, da ni potrebno ustvarjati objekta razreda HelloWorld
    //          (tj. ni nam treba narediti new HelloWorld()),
    //          da lahko klicemo metodo main()
    // void   - pove, da metoda main() ne vraca nicesar
    //
    // (String[] args) - pove, da metoda (/funkcija) main() prejme en argument -
    //                   tabelo nizov (String), ki so v bistvu parametri, ki jih
    //                   navedemo ob zagonu programa
    // Vec o tem na:
    //  https://docs.oracle.com/javase/tutorial/essential/environment/cmdLineArgs.html
    // =========================================================================
    public static void main(String[] args) {
        // lokalna spremenljivka
        //
        // Tu smo jo le deklarirali (povedali smo ime ter tip, ne pa njene
        // vrednosti)
        // Vec o tem: https://www.w3schools.in/java-tutorial/variables/
        String ime;
        
        // Izpisemo besedilo ter gremo v novo vrstico (println)
        System.out.println(besedilo);
        
        // Poklicemo konstruktor razreda Scanner, ki nam naredi objekt tipa Scanner
        //
        // Konstruktor ni nic drugega kot metoda (/funkcija), ki lahko dobi
        // argumente za vhod in vrne objekt kot izhod
        //
        // V primeru konstruktorja Scanner mu za argument podamo vhod,
        // iz katerega bere
        //
        // Vec o konstruktorjih na:
        //  https://www.geeksforgeeks.org/constructors-in-java/
        Scanner interpreterVhoda = new Scanner(System.in);
        
        // Izpisemo vprasanje ter ostanemo v isti vrstici (print)
        System.out.print("Vnesi ime: ");
        // Interpretiramo niz do prvega presledka ter jo shranimo v lokalno
        // spremenljivko ime (in jo s tem inicializiramo)
        ime = interpreterVhoda.next();
        
        // Povprasamo se za stevilo
        System.out.print("Vnesi letnik studija: ");
        
        // Lokalne spremenljivke se lahko v Javi deklarirajo tudi vmes
        // Priporocljivo je, da si spremenljivke deklarirate tam kjer jih
        // boste dejansko uporabljali
        //
        // Spremenljivko lahko hkrati deklariramo in inicializiramo.
        //
        // Scanner.nextInt() bo poskusal interpretirati stevilo do prvega
        // presledka - ce mu to ne uspe, bo vrgel izjemo (Exception)
        int letnikStudija = interpreterVhoda.nextInt();
        
        System.out.println(
            // Izraze lahko porazdelimo cez vec vrstic!
            //
            // Nize (String) v javi lahko zdruzujemo s '+' operatorjem
            // V primeru, ce kaksna vrednost/spremenljivka ni niz, ga bo
            // avtomaticno pretvorilo v niz.
            ime + ", dobrodosel v " + letnikStudija + ". letnik FRI!"
        );
    }
}
