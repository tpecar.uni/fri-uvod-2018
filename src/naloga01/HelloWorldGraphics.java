/*
 *  Za namen uvoda v Studij za bruce FRI 2018.
 *  Vsa koda je pod MIT licenco (glej LICENSE.md)
 */
package naloga01;

import java.awt.Canvas;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

/**
 * Naredi okno z gumbom, ki sprozi "Hello World!" animacijo.
 * 
 * Uporabili bomo Swing graficno okolje, ki je del standardne knjiznice.
 *  https://en.wikipedia.org/wiki/Swing_(Java)
 * 
 * Boste pa nekaj hitro ugotovili: Swing ni ravno namenjen za neke hudo
 * dinamicne vsebine, posledicno bo animacija precej "stekala".
 * 
 * Bolj zvedave glave lahko poskusajo izboljsati primercek s pomocjo sledecih
 * linkov:
 *  http://zetcode.com/tutorials/javagamestutorial/animation/
 *  https://docs.oracle.com/javase/tutorial/uiswing/misc/timer.html
 *  https://pavelfatin.com/low-latency-painting-in-awt-and-swing/
 *  https://www.oracle.com/technetwork/java/painting-140037.html
 *  https://docs.oracle.com/javase/tutorial/extra/fullscreen/bufferstrategy.html
 * Ali pa da poskusite na drugacen nacin risati na povrsino.
 * 
 * Nadaljnje branje:
 *  https://docs.oracle.com/javase/tutorial/uiswing/components/frame.html
 *  https://docs.oracle.com/javase/tutorial/uiswing/components/panel.html
 *  https://docs.oracle.com/javase/tutorial/uiswing/components/button.html
 *  https://docs.oracle.com/javase/tutorial/uiswing/layout/box.html
 *  
 * Upravljanje z dogodki (events)
 *  https://docs.oracle.com/javase/tutorial/uiswing/events/actionlistener.html
 *  (Glej "3. AWT Event-Handling")
 *  https://www3.ntu.edu.sg/home/ehchua/programming/java/j4a_gui.html
 * 
 * Risanje besedila
 *  http://zetcode.com/gfx/java2d/textfonts/
 *  https://docs.oracle.com/javase/tutorial/2d/text/fonts.html
 *  https://docs.oracle.com/javase/tutorial/2d/text/measuringtext.html
 * 
 * Upravljanje z nitmi (threads)
 *  http://tutorials.jenkov.com/java-concurrency/creating-and-starting-threads.html
 */
public class HelloWorldGraphics {
    
    // Tekst, ki ga bomo prikazali v vrstici okna in animirali
    static final String besedilo = "Hello World!";
    
    // Sirina ter visina risalnega okna
    static final int sirina = 320, visina = 120;
    
    
    // Risalna povrsina, na katero bomo risali besedilo
    // Definiramo jo tukaj, da jo bomo lahko delili z Animacija razredom
    // (glej spodaj)
    static RisalnaPovrsina risalnaPovrsina = new RisalnaPovrsina();
    
    
    // Stevilo korakov animacije ter trenutni korak
    //  Spremeljivka korak se uporablja v razredih GumbAkcija ter RisalnaPovrsina
    static final int stKorakov = 100;
    static int korak = 0;
    
    // Dobri stari main
    public static void main(String[] args) {
        
        /*
            Okvirna zgradba nasega okna:

            Okno (JFrame)
            |--------------------------------------------|
            | oknoPovrsina (Container)                   |
            |  |---------------------------------------| |
            |  | risalnaPovrsina (RisalnaPovrsina)     | |
            |  |                                       | |
            |  |   -- besedilo, ki ga izrisujemo --    | |
            |  |                                       | |
            |  |---------------------------------------| |
            |                                            |
            |  |---------------------------------------| |
            |  | gumbPovrsina (JPanel)                 | |
            |  |       |---------------------|         | |
            |  |       |    gumb (JButton)   |         | |
            |  |       |---------------------|         | |
            |  |---------------------------------------| |
            |                                            |
            |--------------------------------------------|
        */
        
        // Ustvarimo novo okno (ko ga ustvaris, je skrito)
        JFrame okno = new JFrame(besedilo);
        // Nastavimo akcijo, ki jo naj okno izvede ob zaprtju
        okno.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
        // Pridobimo povrsino okna
        Container oknoPovrsina = okno.getContentPane();
        // Nastavimo vertikalno razporeditev elementov
        // (risalnaPovrsina, gumbPovrsina) v oknu
        oknoPovrsina.setLayout(new BoxLayout(oknoPovrsina, BoxLayout.Y_AXIS));
        
        // Risalno povrsino smo ze ustvarili kot spremenljivko razreda HelloWorldGraphics
        // Nastavimo minimalno velikost risalne povrsine
        risalnaPovrsina.setPreferredSize(new Dimension(sirina, visina));
        // Dodamo risalno povrsino v okno
        oknoPovrsina.add(risalnaPovrsina);
        
        // Ustvarimo povrsino, ki bo vsebovala gumbe
        JPanel gumbPovrsina = new JPanel();
        // Nastavimo horizontalno razporeditev elementov (gumb) v oknu
        gumbPovrsina.setLayout(new BoxLayout(gumbPovrsina, BoxLayout.X_AXIS));
        // Dodamo povrsino gumbov v okno
        oknoPovrsina.add(gumbPovrsina);
        
        // Ustvarimo gumb in ga dodamo v povrsino gumbov
        JButton gumb = new JButton(besedilo);
        gumbPovrsina.add(gumb);
        
        // V seznam dogodkov, ki se bodo sprozili ob pritisku gumba, dodamo
        // GumbAkcija
        gumb.addActionListener(new GumbAkcija());
        
        // Zahtevamo razporeditev elementov v oknu ter ga prikazemo
        okno.pack();
        okno.setVisible(true);
        
        System.out.println("main() se je zakljucil");
        // Tu se nas main dejansko konca - vendar ko mi z "new JFrame" naredimo
        // okno, se ustvari nova izvajalna nit (thread) programa
        // Program se zakljuci, ko se zakljucijo vse niti
    }
    
    /**
     * Razred, za katerega jamcimo (nakazano z "implements" kljucno besedo),
     * da se ga lahko uporablja kot ActionListener razred.
     * 
     * Da se lahko neka stvar uporablja kot ActionListener, mora imeti metodo
     * actionPerformed(), ki se bo izvedla ob dogodku, na katerega ga vezemo
     * (vezemo ga preko addActionListener() )
     * 
     * 
     * Razred GumbAkcija je vsebovan v razredu HelloWorldGraphics -
     * na ta nacin imamo dostop do atributov (ang. members)
     * HelloWorldGraphics razreda (kot so besedilo, sirina, visina).
     * 
     * Vendar, da lahko GumbAkcija dostopa do staticnih atributov oz.
     * spremenljivk razreda (ang. class variables)
     * HelloWorldGraphics razreda, pa mora biti razred definiran kot staticen
     * (static class).
     * 
     * Vec o tem na:
     *  http://tutorials.jenkov.com/java/nested-classes.html
     *  https://www.developer.com/java/data/understanding-java-nested-classes-and-java-inner-classes.html
     */
    static class GumbAkcija implements ActionListener {
        @Override // Z Override nakazes, da zamenjas obstojeco metodo
                  // (razreda ActionListener) s svojo
                  // Ni potrebno, je pa dobra praksa.
        public void actionPerformed(ActionEvent dogodek) {
            // Ob pritisku gumba se bo izvedla vsebina actionPerformed
            //
            // Naceloma bi radi ob pritisku gumba izvedli animacijo
            // (s spreminjanjem atributa korak), ter ponovno izrisali vsebino
            // risalne povrsine (z metodo paint() v RisalnaPovrsina)
            //
            // Vendar se tu pojavi catch - graficno okolje Swing bo posodobilo
            // vsebino risalne povrsine sele takrat, ko se actionPerformed
            // zakljuci, torej ne moremo cakati v njem
            //
            // main() se je prav tako ze zakljucil, torej ne moremo posodabljati
            // korakov v njem - torej bomo morali ustvariti novo nit ter korak
            // posodabljati tam (to pocne nas razred Animacija - glej spodaj)
            
            Animacija animacija = new Animacija();
            // Kot omenjamo spodaj, razred Animacija razsirja Thread - torej
            // bo razred Animacija imel tudi vse metode razreda Thread, med
            // drugim start(), ki pozene novo izvajalno nit
            animacija.start();
            
        /*
        Okvirno delovanje niti bo s tem:
        ========================================================================

        nit 0:

        main()
          |
          |            nit 1:
          V
        new JFrame() ---> ustvari novo nit Swing ogrodja
          |              |
          |              |
          V              |
        konec main(),    |
        konec niti       | <------------ <----------
                         V             | v zanki   |
                      risanje,         |           |
                  cakanje na dogodke ---           |
                         |                         |
                         V                         |
                   dogodek na gumbu, klicemo       |
                   GumbAkcija.actionPerformed()    |
                         |                         |
                         |                         |          nit 2:
                         |                         |
                  animacija.start() ---------------|----------->  ustvari novo nit razreda Animacija
                         |                         |            |
                         |                         |  klicemo Animacija.run()
                  konec actionPerformed()          |            |
                         |                         |            |
                         |------------->------------            V
                                                            konec run(),
                                                            konec niti

        ========================================================================
        */
        }
    }

    /**
     * Razred, ki razsirja (nakazano z "extends" kljucno besedo) Canvas razred.
     * 
     * Nas razred (RisalnaPovrsina) bo imel vse atribute
     * (tj. spremenljivke objekta) ter metode Canvas razreda, torej lahko zanj
     * jamcimo, da se lahko obnasa vsaj kot Canvas razred - zato ga lahko
     * uporabimo povsod, kjer se pricakuje Canvas razred.
     * 
     * Ker pa ga razsirjamo, lahko poljubno spremenimo obstojece metode
     * razreda, dodamo nove itd. Mi bomo spremenili paint() metodo, ki doloca
     * izris vsebine Canvas objekta.
     * 
     * Vec o tem na:
     *      https://www.tutorialspoint.com/java/java_inheritance.htm
     * 
     * paint() metoda je razlozena na
     *      https://docs.oracle.com/javase/7/docs/api/java/awt/Canvas.html#paint(java.awt.Graphics)
     * 
     * 
     * Nasa animacija bo v bistvu besedilo, ki se zapelje od desnega proti
     * levemu koncu zaslona, zacne in konca pa naj izven vidnega polja.
     * 
     *                        risalnaPovrsina
     *                   |-----------------------|
     *                   |(x: 0, y:0)            |
     *                   |                       |
     *    konec animacije|                       |zacetek animacije
     *                   |                       |
     *         [besedilo]|   {-----------------  |[besedilo]
     *         ^         |     smer premikanja   |^
     *         |                                  |
     *         |                                  zacetna pozicija mora biti na 
     *         |                                  (x: sirina, y: nekaj)
     *         koncna pozicija besedila mora biti na
     *         (x: -sirinaBesedila, y: nekaj)
     *
     */
    static class RisalnaPovrsina extends Canvas {
        
        @Override // Nakazemo, da bomo zamenjali obstojeco metodo paint()
        public void paint(Graphics g) {
            // Pridobimo font (dolocimo pisavo ter velikost)
            Font font = new Font("Dialog", Font.BOLD + Font.ITALIC, 48);
            g.setFont(font);
            
            // Pridobimo dolzino besedila (v pikslih)
            FontMetrics fm = g.getFontMetrics(font);
            int sirinaBesedila = fm.stringWidth(besedilo);
            
            // dolocimo trenutno pozicijo besedila glede na korak animacije
            int besediloX = sirina - (sirina + sirinaBesedila)*korak/stKorakov;
            int besediloY = visina/2;
            
            g.drawString(besedilo, besediloX, besediloY);
        }
    }
    
    /**
     * Razred, ki razsirja (nakazano z "extends" kljucno besedo) Thread razred.
     * 
     * Nas razred Animacija bo imel vse atribute ter metode Thread razreda,
     * torej se ga lahko uporablja povsod, kjer se pricakuje Thread razred.
     * 
     * Nadomestimo metodo run(), da bo premikala korak animacije v lastni
     * izvajalni niti (thread).
     * 
     * Glej se:
     *  https://docs.oracle.com/javase/7/docs/api/java/lang/Thread.html
     */
    static class Animacija extends Thread {
        @Override // Nakazemo, da bomo zamenjali obstojeco metodo run()
        public void run() {
            // Thread.sleep lahko vrze izjemo
            try {
                System.out.println("Zacetek animacije.");
                while(korak < stKorakov) {
                    // Premaknemo korak
                    korak++;
                    
                    // Zahtevamo posodobitev risalne povrsine
                    risalnaPovrsina.repaint();
                    
                    // Pocakamo 50 ms
                    Thread.sleep(50);
                }
                System.out.println("Konec animacije.");
                korak = 0;
            }
            catch(Exception e) {
                System.err.format("Izjema: %s", e.toString());
                e.printStackTrace();
            }
        }
    }
}
