/*
 *  Za namen uvoda v Studij za bruce FRI 2018.
 *  Vsa koda je pod MIT licenco (glej LICENSE.md)
 */
package naloga01;

/**
 * Implementirajmo podporni razred beseda, ki lahko prejme niz ter ga bodisi
 * vrne bodisi izpise.
 * 
 * Nadaljnje branje:
 *  https://www.geeksforgeeks.org/classes-objects-java/
 */
public class HelloWorldRazred {
    
    // Dobri stari main
    public static void main(String[] args) {
        Beseda hello = new Beseda("Hello ");
        Beseda world = new Beseda("world!");
        
        hello.izpisi();
        System.out.println(world.vrni());
        
        // lahko tudi dostopamo direktno do atributov
        hello.beseda = "Hallo";
        world.beseda = " welt!";
        
        System.out.println(hello.vrni() + world.vrni());
    }
}

/**
 * Razred Beseda, ki bo hranil niz.
 * 
 * Razred je sam po sebi le nacrt, ki opise kako bo zgrajeno stanje 
 * (katere "spremenljivke" in katerih tipov) in kaksne bodo operacije,
 * ki se bodo izvajale (metode) nad tem stanjem ("spremenljivkami" razreda).
 * 
 * Objekt (tj. instanca) razreda je primerek, ki je bil narejen po nacrtu
 * razreda. V objektu se je rezerviral prostor za vse "spremenljivke" razreda -
 * te zdaj predstavljajo stanje objekta.
 * 
 * Vec o tem na:
 *  https://www.javatpoint.com/difference-between-object-and-class
 */
class Beseda {
    // "spremenljivka" (oz. pravilneje receno atribut) razreda
    String beseda;
    
    // Konstruktor razreda
    // Konstruktor se poklice ob izvedbi "new Beseda(...)" - v tej tocki nam
    // java prvo alocira prostor za vse atribute, nato pa poklice konstruktor.
    //
    // Naloga konstruktorja je inicializirati vse atribute objekta
    // (torej pripraviti njegovo zacetno stanje).
    //
    //  https://www.geeksforgeeks.org/constructors-in-java/
    Beseda(String beseda) {
        // "this" kljucna beseda pove, da se nanasamo na atribut/metodo
        // objekta, katerega metoda je bila poklicana
        //
        // Vec na: https://docs.oracle.com/javase/tutorial/java/javaOO/thiskey.html
        this.beseda = beseda;
    }
    
    // Metoda, ki izpise besedo
    void izpisi() {
        System.out.print(beseda);
    }
    
    // Metoda, ki vrne besedo
    String vrni() {
        return this.beseda;
    }
}