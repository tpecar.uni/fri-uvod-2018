/*
 *  Za namen uvoda v Studij za bruce FRI 2018.
 *  Vsa koda je pod MIT licenco (glej LICENSE.md)
 */
package naloga01;

import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * Postavi minimalen HTTP streznik, ki ob GET requestu servira "Hello world!".
 * 
 * Nadaljnje branje:
 *  https://en.wikipedia.org/wiki/Hypertext_Transfer_Protocol#HTTP_session
 *  http://tutorials.jenkov.com/java-multithreaded-servers/singlethreaded-server.html
 *  https://www.cs.odu.edu/~mweigle/clemson/courses/cpsc360-f05/lectures/sockets-java.html
 *  https://docs.oracle.com/javase/tutorial/networking/sockets/clientServer.html
 *  https://www.pegaxchange.com/2017/12/07/simple-tcp-ip-server-client-java/
 * 
 * Standardna knjiznica:
 *  Izjeme (Exceptions)
 *      https://docs.oracle.com/javase/tutorial/essential/exceptions/
 *      https://www.geeksforgeeks.org/3-different-ways-print-exception-messages-java/
 *  System.out.format
 *      https://dzone.com/articles/java-string-format-examples
 *  Socket, ServerSocket
 *      https://docs.oracle.com/javase/7/docs/api/java/net/Socket.html
 *      https://docs.oracle.com/javase/7/docs/api/java/net/ServerSocket.html
 *  String
 *      https://docs.oracle.com/javase/7/docs/api/java/lang/String.html
 * 
 */
public class HelloWorldHTTP {
    // Vsaka vrstica glave HTTP odgovora se mora koncati s
    // Carriage Return (CR) + Line Feed (LF) znakoma, naredimo konstanto za to
    //
    // Vse spremenljivke razreda, ki se uporabljajo znotraj main (ki je static),
    // morajo tudi same imeti static kljucno besedo
    //
    // Da vrednost spremenljivke ne moremo spreminjati (je konstanta), nakazemo
    // s final kljucno besedo
    static final String CRLF = "\r\n";
    
    // Glava odgovora, ki ga HTTP streznik poslje odjemalcu
    // Ta mora vsebovati dolzino vsebine (dodamo s String.format)
    static final String glava =
             "HTTP/1.1 200 OK"
       +CRLF+"Content-Type: text/html; charset=UTF-8"
       +CRLF+"Content-Length: %1$d" // %1$d = 1. argument format-a, celo stevilo
       +CRLF+"Accept-Ranges: bytes"
       +CRLF+"Connection: close"
       +CRLF
       +CRLF // pred vsebino je 1 prazna vrstica
    ;
    
    // Vsebina odgovora, ki ga HTTP streznik poslje odjemalcu
    // Dinamicno vsebino dodamo s String.format
    static final String vsebina =
        "<html>"
       +"<head>"
       +"    <title>Hello World!</title>"
       +"</head>"
       +"<body>"
       +"    <h1>Hello World!</h1>"
       +"    <p>"
       +"        Ta stran je bila obiskana %1$d-krat."
       +"    </p>"
       +"</body>"
       +"</html>"
    ;
    
    // Dobri stari main
    public static void main(String[] args) {
        try {
            // Odpremo TCP socket na vratih 8080 (standardno za HTTP streznik)
            ServerSocket server = new ServerSocket(8080);
            
            int steviloObiskov = 1;
            
            // Do konca svojih dni
            while(true) {
                // server.accept bo cakal (blokiral), da se bo povezal odjemalec
                System.out.print("Cakam na odjemalca ... ");
                Socket odjemalec = server.accept();
                System.out.format("%d. obisk\n", steviloObiskov);
                
                // Ko se odjemalec poveze, bi nam moral poslati GET ...,
                // tega ne bomo preverjali - mi mu preprosto posljemo odgovor
                
                // Pripravimo glavo ter vsebino
                // Z .getBytes pretvorimo string v byte tabelo
                byte[] vsebinaIzhod =
                    String.format(vsebina, steviloObiskov).getBytes("UTF-8");
                
                byte[] glavaIzhod =
                    String.format(glava, vsebinaIzhod.length).getBytes("UTF-8");
                
                OutputStream izhod = odjemalec.getOutputStream();
                izhod.write(glavaIzhod);
                izhod.write(vsebinaIzhod);
                
                // prekinemo povezavo z odjemalcem
                odjemalec.close();
                // povecamo stevec obiskov
                steviloObiskov++;
            }
        }
        
        // Tu polovimo vse izjeme, ki se lahko zgodijo tekom izvajanja programa.
        // Naceloma bi za vsak odsek kode, ki lahko vrze izjemo, moral imeti
        // svoj try-catch, vendar za namen demonstracije bomo poenostavili zadeve.
        catch (Exception e) {
            System.err.format("Izjema: %s", e.toString());
            e.printStackTrace();
        }
    }
}
