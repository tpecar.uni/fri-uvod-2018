# 9. Naloga
Napišite program, ki prebere število a in pa zaporedje števil dolžine a. Program naj izpiše povprečje teh števil.

Primer vhoda:
```
5
1 2 3 4 5
```
Primer izhoda: 
```
3
```
