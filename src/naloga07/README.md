# 7. Naloga
Napišite program, ki prebere cela števila a, b in k, nato pa izpiše zaporedje števil od a do b s korakom k.

Primer vhoda:
```
10 20 2
```
Primer izhoda:
```
10 12 14 16 18 20
```
