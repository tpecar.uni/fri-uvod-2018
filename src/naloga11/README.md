# 11. Naloga
Napišite program, ki prebere število a in pa zaporedje števil dolžine a. Izpiše naj ista števila v naraščajočem vrstnem redu. Implementirajte 2 izmed O(n^2) algoritmov (bubble, selection, insertion).

Primer vhoda:
```
5
4 2 5 1 3
```
Primer izhoda: 
```
1 2 3 4 5
```