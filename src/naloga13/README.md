# 13. Naloga
Napišite program, ki prebere binarno število ter njegovo dolžino in ga izpiše v desetiškem sistemu.

Primer vhoda:
```
3
1 1 1
```
Primer izhoda: 
```
7
```
