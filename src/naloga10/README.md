# 10. Naloga
Napišite program, ki prebere število a in pa zaporedje števil dolžine a. Program naj izpiše RAZLICNI, če so vsa števila različna. V nasprotnem primeru naj izpiše prvo število, ki se ponovi.

Primer vhoda:
```
5
1 2 3 4 5
```
Primer izhoda: 
```
RAZLICNI
```