# 3. Naloga
Napišite program, ki prebere tri cela števila in jih izpiše v naraščajočem vrstnem redu.

Primer vhoda:
```
5 2 7
```
Primer izhoda:
```
2 5 7
```