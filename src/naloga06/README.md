# 6. Naloga
Napišite program, ki prebere pozitivno celo število, nato pa izpiše vse njegove delitelje.

Primer vhoda:
```
15
```
Primer izhoda:
```
1 3 5 15
```