# 4. Naloga
Napišite program, ki prebere 2 števili, izpiše pa seštevek, odštevek in zmnožek števil in tako dalje. Lahko dodate se ostale funkcionalnosti (potenca, koren, logaritem ...). Basically kalkulator.

Primer vhoda:
```
5 2
```
Primer izhoda:
```
7 3 10
```
