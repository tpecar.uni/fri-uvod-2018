# 12. Naloga
Napišite program, ki prebere število a in izračuna njegovo fakulteto. Naredite iterativno ter rekurzivno verzijo.

Primer vhoda:
```
5
```
Primer izhoda: 
```
120
```
