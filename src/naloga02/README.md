# 2. Naloga
Napišite program, ki prebere števili a in b, potem pa izpiše katero je večje.

Primer vhoda:
```
2 5
```
Primer izhoda:
```
2 je manjše od 5
```
