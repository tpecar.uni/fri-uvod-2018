# 8. Naloga
Napišite program, ki prebere število a in število b ter izpiše poštevanko števila a s faktorji od 1 do vključno b.

Primer vhoda:
```
5 4
```
Primer izhoda: 
```
5 * 1 = 5
5 * 2 = 10
5 * 3 = 15
5 * 4 = 20
```
