# Kaj je to

To je repozitorij vsega gradiva, ki ga je vam kolega Tadej P. zbral za namen uvoda brucov FRI.

Ce zelite naloge, poglejte v [src mapo](src/)

---

# Nekaj kazalcev kako do odgovorov

## Sprašujte na predavanjih/vajah

Tu ste zato, da se učite. Nobeden ne pričakuje, da boste že znali snov, ki vam jo profesorji/asistenti želijo predavati.
Dostikrat se boste zmotili. Ampak poanta je ravno v tem, da tudi če se zmotite oz. ne veste nečesa, vas lahko drugi popravijo oz. vam stvar razložijo.

Če ne boste rekli ničesar, tudi dobili ne boste nič, ali pa vsaj manj kot bi lahko.

## Learn to Google-fu
1. Će ne veš, vprašaj kolega.
2. Če kolega ne ve, skupaj vprašajata strica Googla oz. katerikoli je že vaš
   najljubši brskalnik

Vendar še vedno se splača obrniti na Google, ko iščemo

* datoteke specifičnega tipa - to lahko iščemo s `[pojem] filetype:končnica_datoteke`
  [primer](https://www.google.com/search?q=data+structures+filetype%3Apdf&ie=utf-8)
* zadetke iz specifičnih strani - to lahko iščemo s `[pojem] site:url_strani`
  [primer](https://www.google.com/search?q=linked+list+site%3Agithub.com)

## Gradivo za Javo

Izogibajte se debelim knjigam, raje vzemite v roke [cheatsheet](https://introcs.cs.princeton.edu/java/11cheatsheet/)
ter poskušajte rešiti nalogo.

Za standardno knjižnjico Jave glejte JDK oz. Oracle dokumentacijo - [primer za String](https://www.google.com/search?&q=string+site%3Ahttps%3A%2F%2Fdocs.oracle.com)

Za ostale gluposti imate pa kolega ali pa Stackoverflow.

## Gradivo za podatkovne strukture

Za podatkovne strukture ter urejanja vam je lahko gradivo [Open Data Structures](https://sl.opendatastructures.org/)
Toliko da veste - ta knjiga je precej obsežna in v bistvu zajema snov APS1 ter del snovi APS2.

Za marsikatere algoritme je v bistvu najboljši resurs youtube, tako da si raje poglejte video in ne zapravljajte časa s knjigami.

## Git

Prej se ga naučite uporabljat, lažje vam bo.

[kaj je to](https://www.youtube.com/watch?v=OqmSzXDrJBk)

[kako se ga uporablja](https://www.youtube.com/playlist?list=PLWKjhJtqVAbkFiqHnNaxpOPhh9tSWMXIF)

## Dodatno gradivo

https://mega.nz/#F!O19lyDhS

za geslo vprasajte kolega.